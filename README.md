# Dotfiles for my center of operations
In this repo, you will find the dotfiles to replicate my workspace, which is centered around:

1. Tiling workflows
2. Vim keybinds
3. Efficiency
4. Completeness
5. Utilitarian minimalism

The goal of this rice is to provide a beautiful **and** useful environment which you _work with_, not _work against_. It aims to be completely controlled by pywal and makes use of Powerline and Nerd Font glyphs for visualization. Of course it leaves room for your own changes: Everything is explained in comments and further documentation will follow.

Countless hours of working, tinkering and nitpicking have gone into this rice, but it still is incomplete. In fact, with the changes and additions planned, this might be the one of the most complete rices you'll see around (don't quote me on that, though!).

Credits where they are due: this rice is mainly based on [this one](https://github.com/VivaCaligula/DOTFILES).

## Programs used
WM: bpswm + sxhkd

Bar: polybar

Editor: Neovim, Spacemacs

Fonts: Hack, awesome-terminal-fonts

Lockscreen: betterlockscreen (soon to be replaced)

Shell: zsh, oh-my-zsh

OMZ theme: agnoster


User programs: 

- Calcurse, a terminal calendar

- gotop, a terminal task manager

- lazygit, a terminal git client

- homesick, a dotfiles manager

- dmenu, rofi

- firefox

- ncmpcpp

- ranger

- terminte, rxvt-unicode-pixbuf (for image previews in ranger)

- thefuck

- compton

- yay

- udiskie, bashmount
