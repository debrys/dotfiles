#=====================#
#     My sxhkdrc.     #
#=====================#


# If you wonder why there is a ``zsh -ic`` after the invocation of ``alacritty -e``:
# It's because the colorscheme won't apply to the newly opened terminal window otherwise.

# Open shortcut helper
super + F1
	zsh -c ~/scripts/keyhelp

# Terminal emulator.
super + Return
	alacritty -e zsh -ic 'neofetch && zsh' # For a nice neofetch at the top

# Terminal emulator, but smaller.
super + shift + Return
	bspc rule -a \* -o state=floating rectangle=500x300+400+250 && alacritty # One shot bspc rule for a floating terminal without neofetch

# Program launcher.
super + space
  rofi -show run
#  dmenu_run -nb "#303030" -nf "#555555" -sb "#222222" -sf "#d97464" -fn Monospace-12:normal

# Make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd


# Switch colorscheme
super + shift + z
	zsh -c ~/scripts/walcycle.sh


###  Bspwm hotkeys.  ###

# Quit bspwm normally.
super + alt + Escape
	bspc quit

# Close and kill.
super + q
	bspc node -c

# Alternate between the tiled and monocle layout.
super + m
	bspc desktop -l next

# Send node to the last preselected area
super + g
	bspc node -n 'last.!automatic'

# swap current node and the biggest one on the current desktop
super + shift + g
	bspc node -s biggest.local


###  State/flags.  ###

# Toggle floating
super + shift + f 
	if [ -z "$(bspc query -N -n focused.floating)" ]; then \
		bspc node focused -t floating; \
	else \
		bspc node focused -t tiled; \
	fi

# Toggle fullscreen 
super + f
	if [ -z "$(bspc query -N -n focused.fullscreen)" ]; then \
		bspc node focused -t fullscreen; \
	else \
		bspc node focused -t tiled; \
	fi

# Make window tiled
super + t
	bspc node focused -t tiled
# Set the node flags.
super + ctrl + {x,y,z}
	bspc node -g {locked,sticky,private}


###  Focus/swap.  ###

# Focus the node in the given direction.
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}


# Focus the node for the given path jump.
#super + {p,b,comma,period}
#	bspc node -f @{parent,brother,first,second}

# Focus the next/previous node.
super + {_,shift + }c
	bspc node -f {next,prev}.local

# Focus the next/previous desktop.
super + bracket{left,right}
	bspc desktop -f {prev,next}

# Focus the last node/desktop.
super + {grave,Tab}
	bspc {node,desktop} -f last

# Focus the older or newer node in the focus history.
#super + {o,i}
#	bspc wm -h off; \
#	bspc node {older,newer} -f; \
#	bspc wm -h on

# Focus or send to the given desktop.
#super + {_,shift + }{1-9,0}
#	bspc {desktop -f,node -d} '^{1-9,10}'
## This was split into the two following commands to follow the sent node.

# Focus to the given desktop.
super + {1-9,0}
	bspc desktop -f '^{1-9,10}'

# Send and follow to given desktop.	
#super + shift + {1-9}
#    id=$(bspc query -N -n); bspc node -d ^{1-9}; bspc node -f ${id}

# Send to given desktop
super + shift + {1-9,0}
	bspc node -d ^{1-9,0}


###  Preselection.  ###

# Preselect the direction.
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# Preselect the ratio.
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# Cancel the preselection for the focused node.
super + ctrl + space
	bspc node -p cancel

# Cancel the preselection for the focused desktop.
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel


###  Move/resize.  ###

# Expand a window by moving one of its side outward.
super + {y,u,i,o}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# Contract a window by moving one of its side inward.
super shift + {y,u,i,o}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# Move a floating window.
super + {Left,Down,Up,Right}
	bspc node -v {-10 0,0 10,0 -10,10 0}

# Gotta go faster! (Same as above, but faster)
super + shift + {Left,Down,Up,Right}
	bspc node -v {-50 0,0 50,0 -50,50 0}

# Rotate the desktop by 90 degrees
super + shift + r
	bspc node @/ -R 90



#====================================#
#  Extra stuff, my configs.  	     #
#====================================#

# Lock the session.
ctrl + shift + x
  bash ~/scripts/lock.sh

# Volume stuff.
ctrl + Up
	amixer sset Master 5%+
ctrl + Down
	amixer sset Master 5%-
super + shift + m
	amixer sset Master 100%-

# Brightness stuff. You usually have a function key for this. If not, your laptop/monitor sucks.
#ctrl + Left
#	brightness down
#ctrl + Right
#	brightness up

# Expand/contract a floating window on all sides.
super + alt + Right
	bspc node -z left -20 0; \
	bspc node -z bottom 0 10; \
	bspc node -z top 0 -10; \
	bspc node -z right 20 0
super + alt + Left
	bspc node -z left 20 0; \
	bspc node -z bottom 0 -10; \
	bspc node -z top 0 10; \
	bspc node -z right -20 0

# Flash the current window
alt + space 
	flash_window



#====================================#
#  Launching programs the cool way.  #
#====================================#


# Normal ranger
super + r
	alacritty -e zsh -ic ranger
	

# spacemacs
super + s
	zsh -c "~/scripts/./em"

# System monitor
super + alt + i
	alacritty -e zsh -ic gotop


# Internet browser
super + w
	firefox

# Music Player
super + n 
	alacritty -e zsh -ic ncmpcpp
