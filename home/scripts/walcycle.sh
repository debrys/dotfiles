#! /bin/sh
#========================================#
#     Caligula's wal recycle script.     #
#========================================#

# Cycle wal background image and colorscheme.
wal -i "$HOME/Pictures/wal"

# Restart polybar to match new colorscheme.
pkill polybar
polybar rome &

# I personally dislike window borders, so I disabled them.
# A red focused_border_color is needed in order to "flash" 
# the currently focused window as seen in the sxhkdrc.
# Uncomment this if you enabled border colors.
# Also, see the bspwmrc file.

#source "$HOME/.cache/wal/colors.sh"
#bspc config normal_border_color $color0
#bspc config focused_border_color $color1
#bspc config active_border_color $color1
#bspc config presel_feedback_color $color1

bspc config focused_border_color \#ff0000
