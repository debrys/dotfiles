#!/bin/bash
#===========================================#
#     Caligula's auto-installer script.     #
#===========================================#

# This is a script for creating and ricing a BSPWM-based pseudo-DE in Arch-Linux.
# The details and purpose of this rice package: snerx.com/winux and snerx.com/snux.
# If you haven't enabled the AUR, xserver, etcetera yet, do that real quick - https://gist.github.com/VivaCaligula/65a0e3489ec110ed08d31027b1065d4d
# Additionally, you can autorun this script remotely by doing:
# $ curl https://raw.githubusercontent.com/VivaCaligula/SCRIPTS/master/installer.sh | bash

# Refresh keyring and repositories.
sudo pacman-key --refresh-keys
sudo pacman -Syu

# Automatically dl + install all the programs you want from the arch repo; this will take a while.
# Replace DM with whatever your prefered DM is (as well as other prefered programs).
sudo pacman --color always --overwrite --noconfirm -S git zip unrar openssh openvpn ruby zsh yay python-pip awesome-terminal-fonts networkmanager networkmanager-openvpn bspwm sxhkd termite termite-terminfo feh compton ranger tmux htop scrot firefox vim powerline-fonts terminus-font vlc phonon-qt5-vlc electrum ffmpeg mpd ncmpcpp cmatrix cool-retro-term dunst shellcheck keybase-gui thinkfinger
# To change out the DM: $ sudo systemctl enable gdm.service

# Automatically DL + install all programs from the AUR; this will take a while.
sudo yay -Syu
# Supports colored output when you enable Color option in the /etc/pacman.conf file.
yay --cleanafter --sudoloop -S w3m imagemagick hsetroot neofetch gtop polybar dmenu2 dunstify betterlockscreen python-pywal i3lock-color-git grub-customizer chkboot nerd-fonts-complete python-tempora python-portend python-cheroot mnemosyne brainworkshop pkgbrowser rxvt-unicode-pixbuf

# Whatever Shell you get, replace zsh with your prefered shell.
#which zsh
#chsh -s /usr/bin/zsh
#sudo chsh -s /usr/bin/zsh
# To check if user has zsh shell active:
tail /etc/passwd

# Bitmap fonts, DL these if pacman's terminus-font doesn't work.
cd ~ || return
git clone https://github.com/Tecate/bitmap-fonts.git
cd ~/bitmap-fonts || return
sudo cp -avr bitmap/ /usr/share/fonts
xset fp+ /usr/share/fonts/bitmap
fc-cache -fv
ln -s ~/.Xresources ~/.Xdefaults
xrdb ~/.Xresources
cd ~ || return
sudo rm -rf bitmap-fonts

# Extra Ruby script to add icons to ls - https://github.com/athityakumar/colorls
gem install colorls
gem install rdoc 
# Program for dotfile management - we'll use this later to sync the dotfiles
gem install homesick

# DL the dotfiles for all the programs (automatically into their right paths) from git repo.
homesick clone rantsomewhere/opcenter

# So that you don't have to "$ sudo wifi-menu" every time.
sudo systemctl enable NetworkManager.service

# To replace vi with nano as the default text editor for commands such as visudo, set the VISUAL and EDITOR environment variables:
export VISUAL=vim
export EDITOR=vim

# Additionally, you should get https://ohmyz.sh/
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
# If you want root to have the same, you will have to repeat this after $ su
# Plugins for oh-my-zsh:
cd "${ZSH_CUSTOM1:-$ZSH/custom}/plugins" || return
git clone https://github.com/arzzen/calc.plugin.zsh.git
git clone https://github.com/zdharma/fast-syntax-highlighting.git
git clone https://github.com/supercrabtree/k.git
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install # make sure this is appended to .zshrc: [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
git clone https://github.com/changyuheng/zsh-interactive-cd.git
# Source the .plugin.zsh files in your ~/.zshrc (done already in Caligula's dotfiles).

# Bspwm won't show Rome or Antium unless you run the riceman.sh script and select one.
# Restart your computer, log in with your new DM into bspwm, and you're done.
systemctl reboot
